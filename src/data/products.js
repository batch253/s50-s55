const productsData = [
		    {
		        id: "wdc001",
		        name: "PHP - Laravel",
		        image: "https://pbs.twimg.com/profile_images/1163911054788833282/AcA2LnWL_400x400.jpg",
		        description: "nostrud id nostrud sint sint deserunt dolore.",
		        price: 45000,
		        onOffer: true
		    },
		    {
		        id: "wdc002",
		        name: "Python - Django",
		        image: "https://d1wrxu8gicsgam.cloudfront.net/wp-content/files/django-logo-big.jpg",
		        description: " ad mollit. Exercitation sunt occaecat labore irure proident consectetur commodo ad anim ea tempor irure.",
		        price: 50000,
		        onOffer: true
		    },
		    {
		        id: "wdc003",
		        name: "Java - Springboot",
		        image: "https://res.cloudinary.com/practicaldev/image/fetch/s--zrUJwvgZ--/c_imagga_scale,f_auto,fl_progressive,h_900,q_auto,w_1600/https://dev-to-uploads.s3.amazonaws.com/uploads/articles/bupbqc9fctvw4j7r14it.png",
		        description: "usmod dolor exercitation dolor mollit duis velit aliquip dolor proident ex exercitation labore cupidatat. Eu aliquip mollit labore do.",
		        price: 55000,
		        onOffer: true
		    },
		     {
		        id: "wdc004",
		        name: "PHP - Laravel",
		        image: "https://pbs.twimg.com/profile_images/1163911054788833282/AcA2LnWL_400x400.jpg",
		        description: "nostrud id nostrud sint sint deserunt dolore.",
		        price: 45000,
		        onOffer: true
		    },
		    {
		        id: "wdc005",
		        name: "Python - Django",
		        image: "https://d1wrxu8gicsgam.cloudfront.net/wp-content/files/django-logo-big.jpg",
		        description: " ad mollit. Exercitation sunt occaecat labore irure proident consectetur commodo ad anim ea tempor irure.",
		        price: 50000,
		        onOffer: true
		    },
		    {
		        id: "wdc006",
		        name: "Java - Springboot",
		        image: "https://res.cloudinary.com/practicaldev/image/fetch/s--zrUJwvgZ--/c_imagga_scale,f_auto,fl_progressive,h_900,q_auto,w_1600/https://dev-to-uploads.s3.amazonaws.com/uploads/articles/bupbqc9fctvw4j7r14it.png",
		        description: "usmod dolor exercitation dolor mollit duis velit aliquip dolor proident ex exercitation labore cupidatat. Eu aliquip mollit labore do.",
		        price: 55000,
		        onOffer: true
		    }
		]
export default productsData;